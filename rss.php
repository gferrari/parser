
<?php 
date_default_timezone_set('America/Argentina/Buenos_Aires');
include "includes/lastRSS.php"; 
include "includes/simple_html_dom.php"; 
$rss = new lastRSS; 
$rss->cache_dir = './temp'; 
$rss->cache_time = 1200; 


$body = '<?xml version="1.0" encoding="UTF-8"?>
<rss version="2.0">
   <channel>
      <title>Peliculas Estrenos</title>
        <link>http://www.guillermoferrari.com.ar/peliculasestrenos/rss.xml</link>
        <description><![CDATA[
            RSS de Proximos Estrenos de peliculas
        ]]></description>
        <ttl>1440</ttl>
        <pubDate>Sun, 04 Aug 2013 00:00:00 -0300</pubDate>
      <lastBuildDate>Sun, 04 Aug 13 23:51:46 -0300</lastBuildDate>';


// Try to load and parse RSS file
if ($rs = $rss->get('http://www.cinesargentinos.com.ar/rss/proximos/')) {
 
    foreach($rs['items'] as $item)
    {
        $html = file_get_html($item['link'].'datoscompletos');                
            //fecha
            $title_original = explode(" ",$item['title']);            
            $date = end($title_original);
            $date = explode("/", $date);
            $title ="";
            //titulo original
            foreach ($title_original as $value) {
                if($value != end($title_original))
                    $title .= $value." ";
            }

            $body .="<item>";
            $title              = htmlspecialchars(trim($title));
            $description        = htmlspecialchars('<img src="'.$html->find('.posterPelicula img',0)->src.'" />'.trim(strip_tags($html->find(".Sinopsis", 0))));
            $pubdate            = str_pad($date[0],2,'0',STR_PAD_LEFT)."-".str_pad($date[1],2,'0',STR_PAD_LEFT).'-'.date("Y");
            $link               = htmlspecialchars($item['link']);

            $body .= "<title>$title</title>\n";
            $body .= "<description>$description</description>\n";
            $body .= "<pubDate>$pubdate</pubDate>\n";                                
            $body .= "<link>$link</link>\n";    
            $body .= "<guid isPermaLink='false'>$link</guid>\n";
            $body .= "</item>\n";            
            echo "$title ... Ok<br>";
        }
    }
else {
    echo "Error: It's not possible to reach RSS file...\n";
}

$body .="</channel></rss>";
$path="rss.xml"; 
$filenum=fopen($path,"w"); 
fwrite($filenum,$body); 
fclose($filenum);
//$body .='<description>'.clearText($item['description']).' <br /><img src="'.$html->find('.posterPelicula img',0)->src.'" /></description>'; 
//$body .="<description><![CDATA[".clearText($html->find(".Sinopsis", 0))."]]</description>";//
?>
